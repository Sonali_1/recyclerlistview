package com.screens.recyclerlistview.BO;

import java.util.ArrayList;

/**
 * Created by sonalisharma on 27/02/18.
 */

public class MovieDescription {

    private String title, thumbnailUrl;
    private int year, id;
    private double rating;
    private String genre;


    public MovieDescription() {

    }

    public MovieDescription(String title, String thumbnailUrl, int year, int id, double rating, String genre) {
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.year = year;
        this.rating = rating;
        this.id = id;
        this.genre = genre;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}


