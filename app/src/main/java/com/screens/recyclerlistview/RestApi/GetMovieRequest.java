package com.screens.recyclerlistview.RestApi;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sonalisharma on 05/03/18.
 */

public class GetMovieRequest extends JsonArrayRequest {

    public GetMovieRequest(String url, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener){
        super(url, listener,errorListener);
    }

}
