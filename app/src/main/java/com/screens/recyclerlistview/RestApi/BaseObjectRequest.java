package RestApi;

/**
 * Created by sonalisharma on 28/02/18.
 */

import com.android.volley.Response;

import org.json.JSONObject;

import java.util.concurrent.Callable;

/**
 * Created by brijesh on 15/10/15.
 */
public abstract class BaseObjectRequest implements Callable<Void>, Response.Listener<JSONObject>, Response.ErrorListener {


    @Override
    public Void call() throws Exception {
        doInBackground();
        return null;
    }

    public abstract void doInBackground();

}