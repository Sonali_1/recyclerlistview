package com.screens.recyclerlistview.RestApi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.screens.recyclerlistview.BO.MovieDescription;
import com.screens.recyclerlistview.R;
import com.screens.recyclerlistview.app.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



/**
 * Created by sonalisharma on 05/03/18.
 */

public class MovieDescriptionAPI extends AppCompatActivity {

    ArrayList<MovieDescription> movieList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      //  makeAPICallToGetMovies();

    }

    public void makeAPICallToGetMovies() {

        movieList.clear();
        String url = "https://api.androidhive.info/json/movies.json";

        /**TODO
         *  Progress bar
         *  code to be in main activity
          */

        GetMovieRequest movieRequest = new GetMovieRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                parseJsonResponse(response);
                Toast.makeText(getApplicationContext(), String.valueOf(movieList.size()), Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        MyApplication.getAppContext().getRequestQueue().add(movieRequest);
    }


    public void parseJsonResponse(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {

            MovieDescription currentMovie = new MovieDescription();
            try {
                JSONObject jsonItem = (JSONObject) response.get(i);
                currentMovie.setTitle(jsonItem.getString("title"));
                currentMovie.setThumbnailUrl(jsonItem.getString("image"));
                currentMovie.setYear(jsonItem.optInt("releaseYear"));
                currentMovie.setRating(jsonItem.optDouble("rating"));
             //   currentMovie.setGenre(convertJsonArrayToArrayList(jsonItem.getJSONArray("genre")));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            movieList.add(currentMovie);
        }
    }

    public ArrayList<String> convertJsonArrayToArrayList(JSONArray response) {

        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            try {
                result.add((String) response.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}


