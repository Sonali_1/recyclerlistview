package com.screens.recyclerlistview.splashscreen;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.screens.recyclerlistview.BO.MovieDescription;
import com.screens.recyclerlistview.R;
import com.screens.recyclerlistview.RestApi.GetMovieRequest;
import com.screens.recyclerlistview.activity.MainActivity;
import com.screens.recyclerlistview.activity.MovieDescriptionListAdapter;
import com.screens.recyclerlistview.app.MyApplication;
import com.screens.recyclerlistview.db.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketPermission;
import java.util.ArrayList;
import java.util.List;

public class MovieDashboard extends AppCompatActivity {

    private List<MovieDescription> movieList = new ArrayList<>();
    private Button showMoviesButton, clearButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_dashboard);
        showMoviesButton = findViewById(R.id.showMovies);
        clearButton = findViewById(R.id.clearDB);

        showMoviesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper db = new DBHelper(getApplicationContext());
                if(db.getAllMovies().size() == 0) {
                    makeAPICallToGetMovies();
                    Toast.makeText(getApplicationContext(), db.getDatabaseName(), Toast.LENGTH_LONG).show();

                } else {
                    openNextActivity();
                }
        }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             DBHelper db = new DBHelper(getApplicationContext());
             db.clearDB();
             Toast.makeText(getApplicationContext(), "DB has been cleared", Toast.LENGTH_LONG).show();
            }
        });

    }


    public void openNextActivity() {

        Intent nextActivity = new Intent(MovieDashboard.this , MainActivity.class);
        startActivity(nextActivity);
    }



    public void updateDatabase() {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        for(int i = 0; i< movieList.size(); i++) {
            dbHelper.addMovie(movieList.get(i));
       //     Toast.makeText(getApplicationContext(), movieList.toString(), Toast.LENGTH_LONG).show();

        }

        openNextActivity();
    }

    public void makeAPICallToGetMovies() {

        String url = "https://api.androidhive.info/json/movies.json";

        GetMovieRequest movieRequest = new GetMovieRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
              //  Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                movieList.clear();
                parseJsonResponse(response);
                updateDatabase();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        movieRequest.setRetryPolicy(new DefaultRetryPolicy(
                4000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyApplication.getAppContext().getRequestQueue().add(movieRequest);
    }

    public void parseJsonResponse(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {

            MovieDescription currentMovie = new MovieDescription();
            try {
                JSONObject jsonItem = (JSONObject) response.get(i);
                currentMovie.setTitle(jsonItem.getString("title"));
                currentMovie.setThumbnailUrl(jsonItem.getString("image"));
                currentMovie.setYear(jsonItem.optInt("releaseYear"));
                currentMovie.setRating(jsonItem.optDouble("rating"));
                ArrayList<String>  genreList = convertJsonArrayToArrayList(jsonItem.getJSONArray("genre"));
                String genreString = generateStringFromArraylist(genreList);
                currentMovie.setGenre(genreString);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            movieList.add(currentMovie);
        }
    }

    public String generateStringFromArraylist(ArrayList<String> arrayList) {

      //Builds mutable memory space for new item to store strings & keep on appending data
      StringBuilder stringBuilder =  new StringBuilder();
        for (int i = 0; i < arrayList.size(); i++)
        {
            if(i  == arrayList.size()-1){

                stringBuilder.append(arrayList.get(i));

            } else {
                stringBuilder.append(arrayList.get(i) );
                stringBuilder.append( " , ");
            }

        }
        return stringBuilder.toString();
    }


    public ArrayList<String> convertJsonArrayToArrayList(JSONArray response) {

        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            try {
                result.add((String) response.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

}

