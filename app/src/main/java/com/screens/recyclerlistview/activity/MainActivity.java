package com.screens.recyclerlistview.activity;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.screens.recyclerlistview.BO.MovieDescription;
import com.screens.recyclerlistview.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.screens.recyclerlistview.RestApi.GetMovieRequest;
import com.screens.recyclerlistview.app.MyApplication;
import com.screens.recyclerlistview.db.DBHelper;


public class MainActivity extends AppCompatActivity {

    private static final int SELECT_PICTURE = 100;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private ImageView imagefromGallery;

    private List<MovieDescription> movieList = new ArrayList<>();
    private MovieDescriptionListAdapter movieDescriptionAdapter;
    Boolean isLinear = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DBHelper dbHelper = new DBHelper(getApplicationContext());

        movieList = dbHelper.getAllMovies();

        recyclerView = findViewById(R.id.movies_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        imagefromGallery = findViewById(R.id.image_from_gallery);

        toolbar = findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);

        initializeListView();
    }
//
//        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
//        floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               // addNewMovieItem();
//                // openImageChooser();
//                Snackbar.make(view, "New Movie has been added", Snackbar.LENGTH_LONG).show();
//            }
//        });
//    }


//    public void showMoviesData() {
//        DBHelper dbHelper = new DBHelper(this);
//        if(result > 0) {
//            dbHelper.getAllMovies();
//        } else {
//            makeAPICallToGetMovies();
//        }
//
//
//
//    }

    public void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the url from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // Get the path from the Uri
                    String path = getPathFromURI(selectedImageUri);

                    // Set the image in ImageView
                   ((ImageView) findViewById(R.id.image_from_gallery)).setImageURI(selectedImageUri);
                   movieDescriptionAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

//    public void addNewMovieItem() {
//        MovieDescription movie = new MovieDescription("Harry Potter", "fdf", 2014, 8.9);
////        movie = new MovieDescription("Alice ", "fdf", 2016, 8.9);
////        movie = new MovieDescription("Bob", "fdf", 2017, 8.9);
////        movie = new MovieDescription("Xmen", "fdf", 2012, 8.9);
////
////        for (int i = 0; i < movieList.size(); i++) {
//            movieList.add(movie);
//            movieDescriptionAdapter.notifyDataSetChanged();
//        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.demolist:
                if (isLinear) {

                    RecyclerView.LayoutManager mlayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                    recyclerView.setLayoutManager(mlayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    isLinear = false;
                    movieDescriptionAdapter = new MovieDescriptionListAdapter(getApplicationContext(), movieList, isLinear);
                    recyclerView.setAdapter(movieDescriptionAdapter);

                } else {
                    RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(mlayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    isLinear = true;
                    movieDescriptionAdapter = new MovieDescriptionListAdapter(getApplicationContext(), movieList, isLinear);
                    recyclerView.setAdapter(movieDescriptionAdapter);
                    updateListView();
                }
                invalidateOptionsMenu();
                break;
        }
        return true;
    }




    public boolean onPrepareOptionsMenu(Menu menu) {

        if (isLinear) {
            menu.getItem(0).setTitle("DemoGrid");
        } else {
            menu.getItem(0).setTitle("Demo List");
        }

        return true;
    }



    public void initializeListView(){

        movieDescriptionAdapter = new MovieDescriptionListAdapter(getApplicationContext(),movieList, isLinear);
        RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mlayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(movieDescriptionAdapter);

    }

    public void updateListView(){

        movieDescriptionAdapter.notifyDataSetChanged();
    }

}
