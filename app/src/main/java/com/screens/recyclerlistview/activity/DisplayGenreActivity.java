package com.screens.recyclerlistview.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.screens.recyclerlistview.R;

import java.util.ArrayList;

public class DisplayGenreActivity extends AppCompatActivity {

    private TextView displayGenreTextview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_genre);


        displayGenreTextview = findViewById(R.id.genreTextview);
        Bundle bundle = getIntent().getExtras();

        String name = bundle.getString("name");
        String genre = bundle.getString("genre");
        displayGenreTextview.setText(genre);

        if (name != null && genre != null ) {
            Toast.makeText(getApplicationContext(), name, Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), genre.toString(), Toast.LENGTH_LONG).show();

        }
    }
}
