package com.screens.recyclerlistview.activity;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.screens.recyclerlistview.BO.MovieDescription;
import com.screens.recyclerlistview.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



import static java.lang.String.valueOf;

/**
 * Created by sonalisharma on 27/02/18.
 */

public class MovieDescriptionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<MovieDescription> movieList;
    private int LIST_TYPE = 0;
    private int GRID_TYPE = 1;

    private Context context;
    private Boolean isLinear = true;

    public MovieDescriptionListAdapter(Context context, List<MovieDescription> movieList, Boolean isLinear) {
        this.movieList = movieList;
        this.context = context;
        this.isLinear = isLinear;
    }

    public MovieDescriptionListAdapter() {}


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == LIST_TYPE) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_descriptor_list_item, parent, false);
            return new ListViewHolder(itemView);

        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.grid_list_item, parent, false);
            return new GridViewHolder(itemView);
        }
    }



    public int getItemViewType(int position) {

        if (isLinear) {
            return LIST_TYPE;
        } else {
            return GRID_TYPE;
        }
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        int viewType = holder.getItemViewType();
        MovieDescription item = movieList.get(position);

        if (viewType == LIST_TYPE) {
            final ListViewHolder listViewHolder = (ListViewHolder) holder;
            MovieDescription movieDescriptionObject = movieList.get(position);
            Picasso.with(context)
                    .load(movieDescriptionObject.getThumbnailUrl()).placeholder(R.drawable.ic_launcher_background)
                    .into(((ListViewHolder) holder).MovieimageView);

            listViewHolder.titleTextView.setText(movieDescriptionObject.getTitle());
            listViewHolder.releaseYearTextView.setText(valueOf(movieDescriptionObject.getYear()));
            listViewHolder.ratingTextView.setText(valueOf(movieDescriptionObject.getRating()));

            listViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent openDetailsIntent = new Intent(context,DisplayGenreActivity.class);
                    openDetailsIntent.putExtra("name",movieList.get(position).getTitle());
                    openDetailsIntent.putExtra("genre", movieList.get(position).getGenre());
                    openDetailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(openDetailsIntent);
                }
            });


        } else {

            GridViewHolder gridViewHolder = (GridViewHolder) holder;
            holder.itemView.setTag(movieList.get(position));
            final MovieDescription movieDescriptionObject = movieList.get(position);
            Picasso.with(context)
                    .load(movieDescriptionObject.getThumbnailUrl())
                    .into(((GridViewHolder) holder).imageView);

            gridViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent openDetailsIntent = new Intent(context,DisplayGenreActivity.class);
                    openDetailsIntent.putExtra("name",movieList.get(position).getTitle());
                    openDetailsIntent.putExtra("genre", movieList.get(position).getGenre());
                    openDetailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(openDetailsIntent);
                }
            });

        }
    }

    public int getItemCount() {
        return movieList.size();
    }


    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView MovieimageView;
        TextView titleTextView, ratingTextView, releaseYearTextView, GenreTextview;
        LinearLayout linearLayout;

        public ListViewHolder(View view) {
            super(view);
            MovieimageView = view.findViewById(R.id.movieimageView);
            titleTextView =  view.findViewById(R.id.titleTextView);
            ratingTextView =  view.findViewById(R.id.ratingTextView);
            linearLayout =  view.findViewById(R.id.linearLayout);
            releaseYearTextView =  view.findViewById(R.id.releaseYearTextView);
            GenreTextview =  view.findViewById(R.id.genreTextview);


        }
    }


    public class GridViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView titleTextView, GenreTextview;
        LinearLayout linearLayout;
        ArrayList<String> Genrelist = new ArrayList<>();

        public GridViewHolder(final View view) {

            super(view);
            imageView = view.findViewById(R.id.grid_item_imageview);
            titleTextView =  view.findViewById(R.id.titleTextView);
            linearLayout =  view.findViewById(R.id.linearLayout);
            GenreTextview =  view.findViewById(R.id.genreTextview);

        }
    }

}

