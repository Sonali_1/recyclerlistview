package com.screens.recyclerlistview.app;

import android.app.Application;
import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.stetho.BuildConfig;
import com.facebook.stetho.InspectorModulesProvider;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.inspector.protocol.ChromeDevtoolsDomain;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.facebook.stetho.rhino.JsRuntimeReplFactoryBuilder;

import okhttp3.OkHttpClient;

/**
 * Created by sonalisharma on 05/03/18.
 */

public class MyApplication extends Application {

    private static MyApplication applicationInstance;
    private RequestQueue requestQueue;

    public static MyApplication getAppContext() { return  applicationInstance ;}
    final Context context = this;
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        applicationInstance = this;

    }


    public RequestQueue getRequestQueue() {
        if(requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());

        }
         return requestQueue;
    }

    public void cancelPendingRequest(Object tag) {
        if(requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }
}
