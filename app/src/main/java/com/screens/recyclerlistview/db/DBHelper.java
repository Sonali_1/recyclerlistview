package com.screens.recyclerlistview.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.screens.recyclerlistview.BO.MovieDescription;
import com.screens.recyclerlistview.activity.MovieDescriptionListAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by sonalisharma on 14/03/18.
 */

public class DBHelper extends SQLiteOpenHelper{

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    @Override
    public String getDatabaseName()
    {
        return super.getDatabaseName();
    }

    private static final String DATABASE_NAME = "moviedb.db";

    // Movie table name
    private static final String MOVIE_TABLE = "movies";

    // Movie Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_RATING  = "ratings";
    private static final String KEY_YEAR  = "year";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_GENRE = "genre";

   MovieDescriptionListAdapter adapter = new MovieDescriptionListAdapter();

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_MOVIE_TABLE = " CREATE TABLE "
                + MOVIE_TABLE + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
                + KEY_NAME + " TEXT NOT NULL , "
                + KEY_IMAGE + " BLOB NOT NULL , "
                + KEY_YEAR + " TEXT NOT NULL , "
                + KEY_RATING + " TEXT NOT NULL , "
                + KEY_GENRE + " TEXT NOT NULL "
                + ")";
        db.execSQL(CREATE_MOVIE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + MOVIE_TABLE);

        // Create tables again
        onCreate(db);
    }


    // Adding new movie
    public void addMovie(MovieDescription movie) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, movie.getTitle());
        values.put(KEY_IMAGE, movie.getThumbnailUrl());
        values.put(KEY_YEAR, movie.getYear());
        values.put(KEY_RATING, movie.getRating());
        values.put(KEY_GENRE, movie.getGenre());

        // Inserting Row
        db.insertWithOnConflict(MOVIE_TABLE, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        db.close();                  // Closing database connection
    }


    //Clear DB
    public void clearDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(MOVIE_TABLE, null, null);
        db.close();
    }


    //List all movies
    public List<MovieDescription> getAllMovies() {

        List<MovieDescription> movieList = new ArrayList<>();


        // Select All Query
        String selectQuery = "SELECT  * FROM " + MOVIE_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MovieDescription movie = new MovieDescription();

                  movie.setId(Integer.parseInt(cursor.getString(0)));
                  movie.setTitle(cursor.getString(1));
                  movie.setThumbnailUrl(cursor.getString(2));
                  movie.setYear(cursor.getInt(3));
                  movie.setRating(cursor.getDouble(4));
                 movie.setGenre(cursor.getString(5));

                movieList.add(movie);
                adapter.notifyDataSetChanged();
            } while (cursor.moveToNext());

        }

        // return movie list
        return movieList;
    }
}

